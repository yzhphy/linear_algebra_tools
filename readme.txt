Linear algebra tools for calling SpaSM in Mathematica for row reduction with finite fields, and lift the rref

Yang Zhang





This package calls the program SpaSM (Charles Bouillaguet, http://cristal.univ-lille.fr/~bouillag/) for row reducing a rational number matrix in finite fields, and then use Chinese remainder theorem for lifting the finite field results to rational numbers.



***************************************    Installation    ***************************************  

1) SpaSM 

To install it, please clone the repository https://github.com/cbouilla/spasm to your local folder. Then type

git pull
autoreconf -i
./configure
make

To use SpaSM, you need to work with matrix in .SMS format. When you have a matrix, you can try for example,

cat [matrix file.sms] | ./rref_gplu | ./rref > [output rref.sms]

SpaSM will first calculate the LU decomposition and then calculate the row reduced echelon form (rref). The result is in output rref.sms . Similarly you can also use SpaSM to calculate the rank or null space ...


2) Mathematica code for row reduction via modular method. Please clone the repository https://bitbucket.org/yzhphy/linear_algebra_tools/src

3) Create a folder in your local computer for temporary files.


***************************************    Usage    ***************************************

For row reduction of a rational number matrix via the modular method:


1) Enter

		SpaSMExchangePath = $HomeDirectory <> "/exchange"
		SpaSMPath = $HomeDirectory <> "/packages/spasm/bench"

in the beginning of your Mathematica code, to set the path for the interface of SpaSM. SpaSMExchangePath is the path for the temporary folder, and SpaSMPath is the path for the subfolder “bench” of your SpaSM package.
  
Then enter,

		Import[$HomeDirectory <> "/packages/Linear_algebra_tools/Farey_fraction/Eculidean_lift.wl"];
		Import[$HomeDirectory <> "/packages/Linear_algebra_tools/Modular_Linear_Algebra/FF_linear_algebra.wl"];.

for loading the Mathematica packages.

2) Use
	    
	        ReducedMatrix = FFRREF[matrix, primeList, MatrixDirectory -> SpaSMExchangePath, Nkernel -> 4];

for row reducing the matrix. Here 'matrix' is the input matrix, 'primeList' should be a list of prime numbers. Nkernel is the number of kernels used for the computation.

My "favourite" prime list is {27697, 27701, 27733, 27737, 27739, 27743, 27749, 27751, 27763, 27767, 27773, 27779, 27791, 27793, 27799, 27803} (16 primes) or {27697, 27701, 27733, 27737, 27739, 27743, 27749, 27751, 27763, 27767, 27773, 27779, 27791, 27793, 27799, 27803, 27809, 27817, 27823, 27827, 27847, 27851, 27883, 27893, 27901, 27917, 27919, 27941, 27943, 27947, 27953, 27961, 27967, 27983, 27997, 28001, 28019, 28027, 28031, 28051} (40 primes).

Note that if the RREF result contains large numerators or denominators, then more prime numbers are needed.

	


