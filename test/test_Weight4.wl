(* ::Package:: *)

(* ::Section:: *)
(*Set up the path *)


SpaSMPath = StringJoin[$HomeDirectory, "/packages/spasm/bench"];    (* This is the path for spasm program *)
Import[$HomeDirectory<>"/packages/linear_algebra_tools/Eculidean_lift.wl"];
Import[$HomeDirectory<>"/packages/linear_algebra_tools/FF_linear_algebra.wl"];
SpaSMExchangePath = StringJoin[$HomeDirectory, "/exchange"];   (* This is the path for temporary files *)


(* ::Section:: *)
(*Example *)


Print[AbsoluteTiming[M=Get[$HomeDirectory<>"/packages/linear_algebra_tools/test/Weight4.txt"];]];   (* Load the matrix *)
Print[M//Dimensions];


primeList=Table[Prime[i],{i,3021,3036}]       
%//Length     (* A list of prime numbers for row reduction *)


RowReduce[M,Modulus->9001]


Print[AbsoluteTiming[RREF=FFRREF[M,primeList,Nkernel->16];]];     (* RREF is the row reduced echlon form *)
(* 436 seconds with 8 cores, 10G RAM usage *)











Mtest=M[[1;;5000,1;;5000]];


Timing[RowReduce[Mtest];]


PolynomialMod[4121243545/1512321321,42013]


AbsoluteTiming[RowReduce[Mtest,Modulus->42013];]



target=113/4021;


FFliftMath[x_Integer,prime_Integer,bound_Integer]:=Module[{a1,a2,b1,b2,q,r1,r2,limit=1/2},
a1=0;
a2=prime;
b1=1;
b2=Mod[x,prime];
If[Abs[b2]<=bound,Return[b2/b1];];
While[b2!=0,
	q=Floor[a2/b2];
	r1=a1-b1 q;
	r2=a2-b2 q;
	If[Abs[r2]<=bound&&0<Abs[r1]<=bound,
		
		If[Abs[r2]/bound<limit&&Abs[r1]/bound<limit,
			Return[r2/r1]
		];
	
		Return[xx];
	];
	a1=b1;
	a2=b2;
	b1=r1;
	b2=r2;
];	
Return[xx];
]


rList=PolynomialMod[target,#]&/@primeList[[1;;3]]



ChineseRemainder[rList,primeList[[1;;3]]]


bound=Sqrt[(Times@@primeList[[1;;3]]-1)/2]//Floor
FFliftMath[4280943326682,Times@@primeList[[1;;3]],bound]


ChineseRemainder[{2,3,2},{3,5,7}]


2^32
