(* ::Package:: *)

Print[AbsoluteTiming[M=Get[$HomeDirectory<>"/packages/linear_algebra_tools/test/Geven4.txt"];]];
Print[M//Dimensions];


M


1445402/(15520*11337)//N


SpaSMExchangePath = StringJoin[$HomeDirectory, "/exchange"]
SpaSMPath = StringJoin[$HomeDirectory, "/packages/spasm/bench"]


Import[$HomeDirectory<>"/packages/linear_algebra_tools/Eculidean_lift.wl"]
Import[$HomeDirectory<>"/packages/linear_algebra_tools/FF_linear_algebra.wl"]


primeList=Table[Prime[i],{i,3021,3036}]
Print[AbsoluteTiming[ReducedMatrix=FFRREF[M,primeList,Nkernel->20];]]; 


(* Export[$HomeDirectory<>"/Dropbox/packages/FF_lift/test/ReduceMatrix.txt",ReducedMatrix//InputForm//ToString]; *)


(* AbsoluteTiming[SparseArray2sms[M,42013,"Aug",Reduction->True];]//Print *)


ReducedMatrix//Dimensions


ReducedMatrix[[1]]//ArrayRules
