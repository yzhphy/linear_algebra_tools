(* ::Package:: *)

(* ::Section:: *)
(*Set up the path *)


SpaSMExchangePath = StringJoin[$HomeDirectory, "/exchange"]    (* This is the path for temporary files *)
SpaSMPath = StringJoin[$HomeDirectory, "/packages/spasm/bench"]    (* This is the path for spasm program *)
Import[$HomeDirectory<>"/packages/linear_algebra_tools/Eculidean_lift.wl"];
Import[$HomeDirectory<>"/packages/linear_algebra_tools/FF_linear_algebra.wl"];


(* ::Section:: *)
(*Trivial Example *)


M=Table[If[RandomReal[]>0.9,1,0],{i,1,200},{j,1,220}];
M=SparseArray[M];


(* Note that for such a small matrix, modular row reduction is actually slower than straightforward row reduction *)


primeList=Table[Prime[i],{i,3021,3060}]   (* prime number list *)


Print[M//Dimensions];


AbsoluteTiming[RREF=FFRREF[M,primeList,Nkernel->8];]//Print


RREF-RowReduce[M]//SparseArray    
