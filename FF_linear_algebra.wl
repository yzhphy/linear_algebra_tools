(* ::Package:: *)

OneThread::usage="An option to determine if SpaSM is running with one thread";


Options[SparseArray2sms]={Reduction->False};
SparseArray2sms[sparseMatrix_,prime_,password_,OptionsPattern[]]:=Module[{ar,m,n,str,s,fileName,file},
	{m,n}=Dimensions[sparseMatrix];
	ar=ArrayRules[sparseMatrix][[1;;-2]];   (* last element {_,_}\[Rule]0  is dropped *)
	(* ar=ar//Sort;  *) (* Necessary? *)
	str=ToString[m]<>" "<>ToString[n]<>" M\n";
	If[OptionValue[Reduction],
		str=str<>StringJoin[ToString[#[[1,1]]]<>" "<>ToString[#[[1,2]]]<>" "<>ToString[PolynomialMod[#[[2]],prime]]<>"\n"&/@ar],
		str=str<>StringJoin[ToString[#[[1,1]]]<>" "<>ToString[#[[1,2]]]<>" "<>ToString[#[[2]]]<>"\n"&/@ar];
	];   (* Reduce over prime or not *)
	str=str<>"0 0 0\n";
	fileName=SpaSMExchangePath<>"/matrix"<>password<>".sms";
	file=OpenWrite[fileName];
	WriteString[file,str];
	Close[file];
];


(* No {_,_}\[Rule]0  is in ar !!! *)
Options[ArrayRules2sms]={Reduction->False};
ArrayRules2sms[ar_,dimensions_,prime_,password_,OptionsPattern[]]:=Module[{m,n,str,s,fileName,file},
	{m,n}=dimensions;
	  (* last element {_,_}\[Rule]0  is dropped *)
	
	str=ToString[m]<>" "<>ToString[n]<>" M\n";
	If[OptionValue[Reduction],
		str=str<>StringJoin[ToString[#[[1,1]]]<>" "<>ToString[#[[1,2]]]<>" "<>ToString[PolynomialMod[#[[2]],prime]]<>"\n"&/@ar],
		str=str<>StringJoin[ToString[#[[1,1]]]<>" "<>ToString[#[[1,2]]]<>" "<>ToString[#[[2]]]<>"\n"&/@ar];
	];   (* Reduce over prime or not *)
	str=str<>"0 0 0\n";
	fileName=Global`SpaSMExchangePath<>"/matrix"<>password<>".sms";
	file=OpenWrite[fileName];
	WriteString[file,str];
	Close[file];
];


Options[smsRead]:={ArrayRulesOnly->False};


smsRead[password_,prime_,OptionsPattern[]]:=Module[{OutFileName,data,m,n,ar,sparseMatrix},
	OutFileName=Global`SpaSMExchangePath<>"/RREF"<>password<>".sms";
	If[!FileExistsQ[OutFileName],Print["Spasm output file does not exist..."];Return[Null]];
	data=Import[OutFileName];	
	m=data[[1,1]];
	n=data[[1,2]];   (* dimension *)
	
	data=data[[2;;-2]]; (* last line 0 0 0  is dropped *)
	ar=Table[{data[[j,1]],data[[j,2]]}->Mod[data[[j,3]],prime],{j,1,Length[data]}];    (* *)
	AppendTo[ar,{_,_}->0];
	If[OptionValue[ArrayRulesOnly],Return[ar]];
	sparseMatrix=SparseArray[ar,{m,n}];
	
	Return[sparseMatrix];
	
];


Options[SpaSMRREF]={OneThread->False,Reduction->False};


SpaSMRREF[sparseMatrix_,prime_,password_,OptionsPattern[]]:=Module[{f,str,InputFileName,OutFileName,timer,redM,ttt=AbsoluteTime[],command},	
	str=SparseArray2sms[sparseMatrix,prime,password,Reduction->OptionValue[Reduction]];
	InputFileName=Global`SpaSMExchangePath<>"/matrix"<>password<>".sms";
	
	OutFileName=Global`SpaSMExchangePath<>"/RREF"<>password<>".sms";
	
	If[FileExistsQ[OutFileName],DeleteFile[OutFileName]];   (* Delete old output file *)
	
	timer=AbsoluteTime[];
	
	(* If[OptionValue[OneThread],
		command="cat "<>InputFileName<>" |  OMP_NUM_THREADS=1   "<>SpaSMPath<>"/RREF --max-recursion 5 --modulus "<>ToString[prime],
		command="cat "<>InputFileName<>" | "<>SpaSMPath<>"/RREF --modulus "<>ToString[prime];
		]; *)
		
	command="cat "<>InputFileName<>" | OMP_NUM_THREADS=30 "<>SpaSMPath<>"/rref_gplu --modulus "<>ToString[prime]<>" | "<>SpaSMPath<>"/rref --modulus "<>ToString[prime]<>" > "<>OutFileName;
		
	Print[command];
	Run[command];
	Print["SpaSM running time: ",AbsoluteTime[]-timer];
	
	redM=smsRead[password,prime];
	Print["RREF total running time: ",AbsoluteTime[]-ttt];
	Return[redM];

];


SpaSMRun[InputFile_,prime_,password_]:=Module[{timer,command,OutFileName},
	timer=AbsoluteTime[];
	OutFileName=Global`SpaSMExchangePath<>"/RREF"<>password<>".sms";
	command="cat "<>InputFile<>" | "<>SpaSMPath<>"/rref_gplu --modulus "<>ToString[prime]<>" | "<>SpaSMPath<>"/rref --modulus "<>ToString[prime]<>" > "<>OutFileName;
	Print[command];
	Run[command];
	Print["SpaSM running time: ",AbsoluteTime[]-timer];
];


(* ::Section:: *)
(*Alternative way of modular RREF*)


CRT[numberList1_,primeList_]:=Module[{numberList},
	numberList=Table[If[!NumericQ[numberList1[[i]]],0,numberList1[[i]]],{i,1,Length[numberList1]}];
	Return[ChineseRemainder[numberList,primeList]];
];


RowNormalization[arRow_]:=Block[{targetList,multiplier},
	multiplier=LCM@@(Denominator[#[[2]]]&/@arRow);
	targetList=Table[arRow[[j,1]]->multiplier*arRow[[j,2]],{j,1,Length[arRow]}];  (* Convert rational numbers to integers *)
	Return[targetList];
];


(* M must be a sparse matrix *)


Options[FFRREF]:={MatrixDirectory->"",FittingLimit->1/1000,Nkernel->4,NSpaSm->4,Tracing->True};
FFRREF[M_,primeList_,OptionsPattern[]]:=Module[{timer,totaltime,AR,i,prime,MR,Mp,path=OptionValue[MatrixDirectory],resultRM,
FileNameHead,fittingLimit=OptionValue[FittingLimit],ARList,keys,dataList,DataMatrix,primeProd,bound,Result,dimM,RMList,targetList,IntegerAR={},inputFile,multiplier,ttt,len,M1},
	timer=totaltime=AbsoluteTime[];
	
	(* *********** Array Rules *********** *)
	
	FileNameHead="matrix";
	dimM=Dimensions[M];
	AR=ArrayRules[M];
	If[OptionValue[Tracing],Print["Array Rules found... ",AbsoluteTime[]-timer]];
	
	(* *********** Integer matrix *********** *)
	
	timer=AbsoluteTime[];
	AR=SplitBy[AR,#[[1,1]]&][[1;;-2]];    (* Row by row *)
	IntegerAR=Table[RowNormalization[AR[[i]]],{i,1,Length[AR]}];
	(* For[i=1,i<=Length[AR],i++,
		targetList=AR[[i]];
		multiplier=LCM@@(Denominator[#[[2]]]&/@targetList);
		targetList=Table[targetList[[j,1]]->multiplier*targetList[[j,2]],{j,1,Length[targetList]}];  (* Convert rational numbers to integers *)
		AppendTo[IntegerAR,targetList];
	]; *)
    IntegerAR=Append[Flatten[IntegerAR],{_,_}->0]; 
    M1=SparseArray[IntegerAR,dimM]; 
	Clear[AR];
	Clear[IntegerAR];
	If[OptionValue[Tracing],Print["Convert the matrix to an integer matrix... ",AbsoluteTime[]-timer]];
	
	
	
	(* *********** Run SpaSM *********** *)
	timer=AbsoluteTime[];
	
	(* ArrayRules2sms[IntegerAR,dimM,9001,"_Azurite"]; *)  (*   9001 is a dumb number which is not used for the sms file *)
	
    SparseArray2sms[M1,9001,"_Azurite"];    
	inputFile=Global`SpaSMExchangePath<>"/matrix"<>"_Azurite"<>".sms";
	LaunchKernels[OptionValue[Nkernel]];
	DistributeDefinitions[inputFile,primeList,SpaSMRun];
	
	ParallelDo[SpaSMRun[inputFile,primeList[[i]],primeList[[i]]//ToString],{i,1,Length[primeList]}];
	If[OptionValue[Tracing],Print["SpaSM finished... ",AbsoluteTime[]-timer]];
	CloseKernels[];
	Clear[M1];
	
	(* *********** Import the data *********** *)
	timer=AbsoluteTime[];
	ttt=AbsoluteTime[];
	LaunchKernels[OptionValue[Nkernel]];
	DistributeDefinitions[primeList,smsRead];
	ARList=ParallelTable[smsRead[primeList[[i]]//ToString,primeList[[i]],ArrayRulesOnly->True],{i,1,Length[primeList]}];
	(* Print["check point 2, ",AbsoluteTime[] -ttt]; *)
	(* Print[MemoryInUse[]]; *)
	ttt=AbsoluteTime[];
	
	(* DistributeDefinitions[RMList,primeList]; *)
	(* ARList=ParallelTable[RMList[[i]]//ArrayRules,{i,1,Length[primeList]}]; *)
	(* Print["check point 3, ",AbsoluteTime[] -ttt]; *)
	(* Print[MemoryInUse[]]; *)
	
	(* Print["clearing ...",MemoryInUse[]]; *)

	ttt=AbsoluteTime[];
	keys=Union@@(Table[Keys[ARList[[i]]],{i,1,Length[primeList]}]);	(* keys for all non-vanishing elements for the reduced matrix *)
	keys=Complement[keys,{{_,_}}];	
	(* Print["check point 4, ",AbsoluteTime[] -ttt]; *)

	ttt=AbsoluteTime[];
	DataMatrix=(keys/.Dispatch[#])&/@ARList;
	
	(* Print[Length/@ARList]; *)
	ClearAll[ARList];
	DataMatrix=DataMatrix//Transpose;   (* Each row is the FF result list of one element *)

	If[OptionValue[Tracing],Print["all data points loaded ... ",AbsoluteTime[]-timer]];
	(* Print[MemoryInUse[]];	 *)
	(* Print["check point 5, ",AbsoluteTime[] -ttt]; *)
	
	(* *********** Lifting *********** *)
	
	timer=AbsoluteTime[];
	primeProd=(Times@@primeList);
	bound=Sqrt[(primeProd-1)/2]//Floor;
	Print["Start to lift the numbers."];
	Print[Length[DataMatrix]," numbers to be lifted."];

    
	len=Length[DataMatrix];
	Result=ParallelTable[FFliftMath2[CRT[DataMatrix[[i]],primeList],primeProd,bound],{i,1,len}];
	
	Print["Lifting finished... ",AbsoluteTime[]-timer];
	resultRM=SparseArray[Append[MapThread[#1->#2&,{keys,Result}],{_,_}->0],Dimensions[M]];
	CloseKernels[];
	
	If[Variables[#[[2]]&/@ArrayRules[resultRM]]!={},
		Print["lift failed. Please use more prime numbers"];
		Return[];
	];
	
	Return[resultRM];
];



(* ::Section:: *)
(*Other tools*)


FindPivots[matrix_]:=Module[{ARLonglist},
	ARLonglist=GatherBy[ArrayRules[matrix],#1[[1,1]]&];
	Return[(#1[[1,1,2]]&)/@ARLonglist[[1;;-2]]];
]


FindNullSpace[m_]:=Module[{a,b,ColumnIndex,PivotIndex,NonPivotIndex,AR,resultAR,NonPivotIndexShuffle},
	(* m must be a RREF sparse matrix *)
	{a,b}=Dimensions[m];
	ColumnIndex=Range[b];
	
	PivotIndex=FindPivots[m];
	NonPivotIndex=Complement[ColumnIndex,PivotIndex];
	NonPivotIndexShuffle=Table[NonPivotIndex[[i]]->i,{i,1,Length[NonPivotIndex]}]//Dispatch;
	
	
	AR=ArrayRules[m][[;;-2]]; (* remove _,_ *)
	resultAR=Select[AR,(MemberQ[NonPivotIndex,#[[1,2]]])&];

	resultAR=resultAR/.({i_,j_}):>({PivotIndex[[i]],j});
	
	resultAR=Join[{#,#}->-1&/@NonPivotIndex,resultAR];
	
	resultAR=resultAR/.({i_,j_}):>({i,j/.NonPivotIndexShuffle});  (* Remove zero columns *)
	

	Return[SparseArray[resultAR,{b,NonPivotIndex//Length}]];
];



