(* ::Package:: *)

FareyFractions[n_]:=Union[Flatten[Table[{a/b,-a/b},{a,0,n},{b,1,n}]]]


nBound[p_]:=Floor[Sqrt[(p-1)/2]];


FFliftMath2[x_Integer,prime_Integer,bound_Integer]:=Module[{a1,a2,b1,b2,q,r1,r2,limit=1/1000},
a1=0;
a2=prime;
b1=1;
b2=Mod[x,prime];
If[Abs[b2]<=bound,Return[b2/b1];];
While[b2!=0,
	q=Floor[a2/b2];
	r1=a1-b1 q;
	r2=a2-b2 q;
	If[Abs[r2]<=bound&&0<Abs[r1]<=bound,
		If[Abs[r2]/bound<limit&&Abs[r1]/bound<limit,
			Return[r2/r1]
		];
	
		Return[xx];
	];
	a1=b1;
	a2=b2;
	b1=r1;
	b2=r2;
];	
Return[xx];
]
